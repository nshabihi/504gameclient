﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class profileMaker : MonoBehaviour {
	private User me;//User whose profile is being visited now
	public User[] friends;
	public Text nameText, panelText;
	public Button itemPrefab,followButton;
	public GameObject followersPanel, followeesPanel;
	private int type;//badges=1, followers=2, followees=0
	private Image[] badges;
	public GameObject badgesPanel;
	public GameObject loadingPanel;
	private string[] titles = {"FOLLOWINGS","TROPHIES","FOLLOWERS"};
	// Use this for initialization
	void Start () {		
		me = codeMother.cmInstance.friend;
		nameText.text = me.name;
		if(me.IFollowHim)
			followButton.GetComponentsInChildren<Text>()[0].text = "Following";
		else
			followButton.GetComponentsInChildren<Text>()[0].text = "Follow me";
		followButton.GetComponentsInChildren<Text>()[1].text = me.id;
		Debug.Log (me.ToString());
		type = 1;
		changePanel (type);
		getFollows (me.id, "getFollowers");
		getFollows (me.id, "getFollowees");
		getAndPlaceBadges (me.id);
	}
	void FixedUpdate(){
		if(Input.GetKeyDown(KeyCode.Escape))
			SceneManager.LoadScene ("leaderboardScene");	
	}
	/**
	 * 
	 * */
	public void getFollows(string userId, string action){		
		HttpResponse htre = new HttpResponse ();
		string[] parms = { "userid=" + userId ,"username="+codeMother.cmInstance.username,"deviceId="+codeMother.cmInstance.loginID};
		StartCoroutine(getFollowsRoutine("Follow",action, parms, htre));
	}


	public IEnumerator getFollowsRoutine(string controller, string action, string[] parms, HttpResponse htre){
		yield return codeMother.cmInstance.sendHttpRequest (controller, action, parms, htre);
		string response = htre.text;
		string[] userStrings = response.Split (';');
		List<User> users = new List<User> ();
		foreach (string userString in userStrings) {
			if(userString.Equals(""))
				break;
			string[] userItems = userString.Split(',');
			User user = new User (userItems [0], userItems [2], userItems [1], userItems [3], userItems [4].Equals("1"));
			Debug.Log (user.ToString ());
			users.Add (user);
		}					
		friends = users.ToArray ();
		RectTransform rowRectTransform = itemPrefab.GetComponent<RectTransform>();
		RectTransform followeeRectTransform = followeesPanel.GetComponent<RectTransform>();
		RectTransform followerRectTransform = followersPanel.GetComponent<RectTransform>();
		foreach (User friend in friends) {
			Button newItem = Instantiate(itemPrefab) as Button;
			newItem.name = gameObject.name + " item at ";
			newItem.GetComponentsInChildren<Text> () [0].text = friend.name;
			newItem.GetComponentsInChildren<Text> () [1].text = friend.score;
			newItem.GetComponentsInChildren<Text> () [2].text = friend.level;
			newItem.GetComponentsInChildren<Text> () [3].text = friend.id;
			if (friend.id.Equals(codeMother.cmInstance.myID)) {
				newItem.GetComponentsInChildren<Button> () [1].image.color = Color.gray;
				newItem.GetComponentsInChildren<Button> () [1].interactable = false;
			} else if (friend.IFollowHim)
				newItem.GetComponentsInChildren<Button> () [1].image.color = Color.green;
			else
				newItem.GetComponentsInChildren<Button> () [1].image.color = Color.white;
			if (action.Equals ("getFollowers"))
				newItem.transform.SetParent (followersPanel.transform);
			else if (action.Equals ("getFollowees"))
				newItem.transform.SetParent (followeesPanel.transform);
			else
				Debug.LogError ("wtf");
		}
	}//getFollowsRoutine

	public void follow(Button button){
		string whatToDo = button.GetComponentsInChildren<Text>() [0].text;
		string userid = button.GetComponentsInChildren<Text>()[1].text;
		HttpResponse htre = new HttpResponse ();
		string[] parms = { "follower="+codeMother.cmInstance.myID,"followee="+userid};

		if(whatToDo.ToLower().Equals("follow me"))
			StartCoroutine(followRoutine("Follow","follow", parms, htre, button));
		else
			StartCoroutine(followRoutine("Follow","unfollow", parms, htre, button));
	}
	/**
	 * Depending on action, sends a follow or unfollow request
	*/
	public IEnumerator followRoutine(string controller, string action, string[] parms, HttpResponse htre, Button button){
		yield return codeMother.cmInstance.sendHttpRequest (controller, action, parms, htre);
		if (htre.text.Equals ("0")) {
			Debug.Log (htre.text);	
			if (button.GetComponentsInChildren<Text> () [0].text .ToLower ().Equals ("following")) {
				Debug.Log ("text was " + button.GetComponentsInChildren<Text> () [0].text + "and will be Followe me");
				button.GetComponentsInChildren<Text> () [0].text = "Follow Me";
			} else if (button.GetComponentsInChildren<Text> () [0].text .ToLower ().Equals ("follow me")) {
				Debug.Log ("text was " + button.GetComponentsInChildren<Text> () [0].text + "and will be Following");
				button.GetComponentsInChildren<Text> () [0].text = "Following";
			}
		}
	}
	/**
	 * starts a coroutine to get badges of user whose id is given as input, and places them into the badges panel
	*/
	public void getAndPlaceBadges(string userId){

		string [] parms = {"userid=" + userId,"username="+codeMother.cmInstance.username,"deviceId="+codeMother.cmInstance.loginID};
		HttpResponse htre = new HttpResponse ();
		StartCoroutine (getAndPlaceBadgesRoutine ("Badge","getUserBadges",parms,htre));

	}
	public IEnumerator getAndPlaceBadgesRoutine(string controller, string action, string[] parms, HttpResponse htre){
		yield return codeMother.cmInstance.sendHttpRequest (controller, action, parms, htre);
		string[] badgeIds = htre.text.Split (',');
		badges = badgesPanel.GetComponentsInChildren<Image> ();
		int badgeId = 1;
		foreach (Image badge in badges) {
			if (badge.name.Equals ("badge") && !codeMother.searchItemInArray (badgeIds, badgeId.ToString())) {
				badge.color = new Color32 (40, 40, 40, 215);
			}
			badgeId++;
		}
	}
	public void ChangeHandler(string direction){
		if (direction.Equals("left")) {			
			type = (type + 2) % 3;
		}
		else if (direction.Equals("right")) {
			type = (type + 1) % 3;
		}
		changePanel (type);
	}

	void changePanel(int type){
		Debug.Log ("changing to panel: " + type);
		followeesPanel.SetActive(false);
		followersPanel.SetActive(false);
		badgesPanel.SetActive(false);
		panelText.text = titles [type];
		if (type == 1) {
			badgesPanel.SetActive(true);
		} else if (type == 2) {
			followersPanel.SetActive(true);
		} else if (type == 0) {
			followeesPanel.SetActive(true);
		} else {
			Debug.Log ("wtf");
		}
	}
	public void userClicked(Button btn){
		codeMother.cmInstance.friend.name = btn.GetComponentsInChildren<Text> () [0].text;
		codeMother.cmInstance.friend.score = btn.GetComponentsInChildren<Text> () [1].text;
		codeMother.cmInstance.friend.level = btn.GetComponentsInChildren<Text> () [2].text;
		codeMother.cmInstance.friend.id = btn.GetComponentsInChildren<Text> () [3].text;
		codeMother.cmInstance.friend.IFollowHim = btn.GetComponentsInChildren<Button> () [1].image.color.Equals(Color.green);
		Debug.Log ("You want to see user profile with id= "+ codeMother.cmInstance.friend);	
		SceneManager.LoadScene ("profileScene");
	}

	public void followFromList(Button button){
		string whatToDo = (button.GetComponentsInChildren<Button>() [1].image.color.Equals(Color.green))?"unfollow":"follow";
		string userid = button.GetComponentsInChildren<Text>()[3].text;
		HttpResponse htre = new HttpResponse ();
		string[] parms = { "follower="+codeMother.cmInstance.myID,"followee="+userid};			
		Debug.Log(whatToDo+" "+ codeMother.cmInstance.myID+" "+userid);
		StartCoroutine(followFromListRoutine("Follow",whatToDo, parms, htre, button));
	}

	/**
	 * Depending on action, sends a follow or unfollow request
	*/
	public IEnumerator followFromListRoutine(string controller, string action, string[] parms, HttpResponse htre, Button button){
		yield return codeMother.cmInstance.sendHttpRequest (controller, action, parms, htre);
		if (htre.text.Equals ("0")) {
			Debug.Log (htre.text);	
			if (button.GetComponentsInChildren<Button> () [1].image.color.Equals(Color.green)){
				Debug.Log ("check unfollow of "+parms[0]+" and "+parms[1]);
				button.GetComponentsInChildren<Button> () [1].image.color = Color.white;
			} else {
				Debug.Log ("check follow of "+parms[0]+" and "+parms[1]);
				button.GetComponentsInChildren<Button> () [1].image.color= Color.green;
			}
		}
	}
}