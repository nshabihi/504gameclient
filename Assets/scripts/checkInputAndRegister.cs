﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Net;
using System.IO;
using UnityEngine.Networking;
using Mono.Data.Sqlite;
using UnityEngine.SceneManagement;
using System.Text.RegularExpressions;

	public class checkInputAndRegister : MonoBehaviour {
	public InputField nameInput;
	public InputField mailInput;
	public Button submitButton;
	public Text errorText;
	public GameObject loadingPanel;
	private SqliteConnection conn;
	private SqliteDataReader rdr;
	// Use this for initialization
//	private codeMother codeMother.cmInstance;
	private string username;
	void Start () {
		Debug.Log("hOy");
		loadingPanel.SetActive (false);
//		codeMother.cmInstance = GameObject.FindObjectOfType<codeMother> ();
	    conn = codeMother.cmInstance.connectDB ();
		SqliteCommand command = new SqliteCommand ("Select * from userInfo",conn);
		rdr = command.ExecuteReader ();
		if (rdr.Read ()) {//already registered		
			Debug.Log (rdr ["personality"]);
			codeMother.cmInstance.setAuthParams ((string)rdr ["name"], (string)rdr ["userUniqueId"]);
			codeMother.cmInstance.myID = rdr ["id"].ToString();
			Debug.Log ("Registered user named: " + codeMother.cmInstance.username + " loginID: " + codeMother.cmInstance.Auth);
			if (rdr ["personality"].Equals ("NULL") || rdr ["personality"] == null) {//bi-shakhsiat!
				SceneManager.LoadScene ("PersonalityScene");
			} else {	
				HttpResponse httre = new HttpResponse ();
				string[] parms = new string[2];
				parms [0] = "username=" + codeMother.cmInstance.username;
				parms [1] = "deviceId=" + codeMother.cmInstance.loginID;
				StartCoroutine(login("Authentication", "login", parms, httre));
				Debug.Log ("waiting");
			}
			codeMother.cmInstance.disconnectDB (rdr, conn, command);
		} else {
			codeMother.cmInstance.disconnectDB (rdr, conn, command);
			Debug.Log ("not registered");
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void chackInputExists(){
		username = nameInput.text;
		if (username.Equals (""))
			return;
		if (!Regex.IsMatch(username, @"^[a-zA-Z]+$")) {
			errorText.text = "Please try another name. use English characters and numbers only";
			return;
		}			
		errorText.text = "Hello " + username;
		Debug.Log("user name: "+nameInput.text);
		loadingPanel.SetActive (true);
		StartCoroutine(GetText());
	}

	IEnumerator GetText() {
		HttpResponse httpResponse = new HttpResponse ();
		string[] parms = new string[2];
		parms [0] = "username=" + username;
		if(!mailInput.text.Equals(""))
			parms [1] = "email=" + mailInput.text;
		yield return codeMother.cmInstance.sendHttpRequest ("Authentication", "register", parms, httpResponse);
		if (httpResponse.success) {
			loadingPanel.SetActive (false);
			string response = httpResponse.text;		
			if (response.Equals ("2Invalid Email")) {
				errorText.text = "Invalid email, please enter a valid email";
			} else if (response.Equals ("3email is userd before")) {
				errorText.text = "email already in use, please enter another email";
			} else if (response.Equals ("4Failed to create user")) {
				errorText.text = "unknown error! please try again later.";
			} else {
				Debug.Log (response);
				conn = codeMother.cmInstance.connectDB ();
				string id = response.Split (',') [7].Split ('\"') [0];
				string uniqueId = response.Split (',') [1];
				string loginId = response.Split (',') [5];
				SqliteCommand command = new SqliteCommand ("insert into userInfo(id,name,userUniqueId,userLoginId) values(\"" + id + "\",\"" + username + "\",\"" + uniqueId + "\",\"" + loginId + "\")", conn);
				Debug.Log (command.CommandText);
				rdr = command.ExecuteReader ();
				codeMother.cmInstance.loginID = uniqueId;
				codeMother.cmInstance.username = username;
				SceneManager.LoadScene ("PersonalityScene");
			}
		} else {
			loadingPanel.SetActive (false);
			errorText.text = "Connection error";
		}
	}
	private IEnumerator login(string controller, string action, string[] ps, HttpResponse htre){
		yield return codeMother.cmInstance.sendHttpRequest (controller, action, ps, htre);
		codeMother.cmInstance.setLoginResponse (htre.text);
		SceneManager.LoadScene ("levelsScene");				
	}
}