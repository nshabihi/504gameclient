﻿using UnityEngine;
using System.Collections;

public class BadgeSceneManager : MonoBehaviour {
	int type;//total=1, league=2, recent=3
	bool friends;
	RowContentHolder rch;
	// Use this for initialization
	IEnumerator Start () {
		rch = GameObject.FindObjectOfType<RowContentHolder> ();
		friends = true;
		yield return rch.GetLeaderboard ("total","0");
	}	
	public void leaderboardChangeHandler(string direction){
		if (direction.Equals("left")) {
			type = (type +2) % 3;//(type+2)%3 is equal to (type-1)%3 but it never returns -1
 			StartCoroutine(rch.GetLeaderboard (getTypeString(type),"1"));
		}
		else if (direction.Equals("right")) {
			type = (type + 1) % 3;
			Debug.Log ("getting new lb info " + getTypeString (type));
			StartCoroutine(rch.GetLeaderboard (getTypeString(type),"0"));
		}
	}
	IEnumerator changeLeaderboard(string ltype, string f){
		yield return rch.GetLeaderboard (ltype,f);
	}
	public string getTypeString(int type){
		if (type == 0)
			return "total";
		else if (type == 1)
			return "myLeague";
		else if (type == 2)
			return "lastTimePeriod";
		return "wtf";//should never happen
	}
}
