﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
/**
 * This class manages the badges, highlights the ones user has won and disables the others
 * */
public class BadgePlacer : MonoBehaviour {
	public Button sampleButton;
	private RowContentHolder rch;
	private Image[] badges;
	public GameObject panel;
	public Text title;
	// Use this for initialization
	public void Start () {		
		
		badges = panel.GetComponentsInChildren<Image> ();
		int badgeId = 1;
		foreach (Image badge in badges) {
			if (!badge.name.Equals ("Panel") && !codeMother.cmInstance.hasBadge (badgeId)) {
				badge.color = new Color32 (40, 40, 40, 215);
				Debug.Log ("does not have "+ badge.name+", "+badgeId);
			}
			badgeId++;
		}
	}
}
