﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

[System.Serializable]
public class RowContent{
	public int rank, level, score,id;
	public string name;
	public bool me = false;
	public int iFollowHim;
	public override string ToString(){
		return "rank: " + rank + ", level: " + level + ", score: " + score + ", id: " + id + ", name: " + name+", follow: "+iFollowHim;
	}
}
public class RowContentHolder : MonoBehaviour {
	public RowContent[] rows;
	private HttpResponse httpResponse;
	private codeMother cm;
	private int myRank, myLevel, myScore;
	private string myName;
	public string lbTitle;
	public GameObject loadingPanel, lbPanel;
	/**
	 * This function sends an http request to get leaderboard data and loads the data to "rows" array after parsing them
	 * "ltype" must be on of these: {total, league, }
	 * "friends" parameter must be zero or one
	 * */
	public IEnumerator GetLeaderboard(string ltype, string friends) { 
		Debug.Log ("starting RowContentHolder");
		cm = GameObject.FindObjectOfType<codeMother> ();
		string[] parameters = new string[3];
		parameters [0] = cm.Auth;
		parameters [1] = "ltype="+ltype;
		parameters [2] = "friends=" + friends;
		httpResponse = new HttpResponse ();
		yield return cm.sendHttpRequest ("Leaderboard", "getLeaderboardWithFriends", parameters,httpResponse);
		Debug.Log (httpResponse.text);
		if (httpResponse.success && httpResponse.text.StartsWith("\"4")) {
			string[] rawResponseArray = httpResponse.text.Split (',');
			int usersInLeaderboard = (rawResponseArray.Length - 6) / 5+1;
			myRank = Convert.ToInt32 (rawResponseArray [4]);
			myName = rawResponseArray [2];
			myLevel = Convert.ToInt32 (rawResponseArray [3]);
			myScore = Convert.ToInt32 (rawResponseArray [3]);
			int rank = 1, j = 0;
			rows = new RowContent[Math.Max(usersInLeaderboard,1)];
			for (int i = 5; i < rawResponseArray.Length && usersInLeaderboard!=0; i++) {
				try {
					RowContent row = new RowContent ();
					row.rank = rank++;
					row.id = Convert.ToInt32 (rawResponseArray [i++]);
					row.name = (rawResponseArray [i++]);
					row.score = Convert.ToInt32 (rawResponseArray [i++]);
					row.level = Convert.ToInt32 (rawResponseArray [i++]);
					row.iFollowHim = Convert.ToInt32 (rawResponseArray [i]);
					Debug.Log (row.ToString ());
					rows [j++] = row;
				} catch (FormatException x) {
					Debug.Log ("exception on i=" + i + " where length is: " + rawResponseArray.Length + x);
				}
				catch(IndexOutOfRangeException ex){
					Debug.Log ("exception on i=" + i + " where length is: " + rawResponseArray.Length + ex);
				}
			}
			if (myRank > usersInLeaderboard) {
				RowContent row = new RowContent ();
				row.rank = myRank;
				row.id = Convert.ToInt32 (cm.myID);
				row.name = myName;
				row.score = myScore;
				row.level = myLevel;
				row.me = true;
				rows [j] = row;
			}
			lbTitle = getLBTitle (ltype,parameters[2].Equals("friends=1"));
			GameObject.FindObjectOfType<RowPlacer> ().PlaceRows ();
		} else {
//			lbPanel.SetActive (false);
			loadingPanel.SetActive (true);
			loadingPanel.GetComponentInChildren<Button> ().interactable = true;
			loadingPanel.GetComponentInChildren<Text> ().text = "Connection failed";
			foreach(Image image in loadingPanel.GetComponentsInChildren<Image> ()){
				if (image.name.Equals ("animatedImage")) {
					image.enabled = false;
				}	
			}
		}
	}
	 
	public bool hasSucceeded(){
		return httpResponse.success;
	}
	public string getLBTitle(string ltype, bool friends){
		if (ltype.Equals ("total"))
			return "TOTAL "+((friends)?"with friends":"global");
		if (ltype.Equals ("myLeague"))
			return "LEAGUE";
		if (ltype.Equals ("lastTimePeriod"))
			return "LAST 3 DAYS "+((friends)?"with friends":"global");
		return "wtf";//should never happen
	}
}
