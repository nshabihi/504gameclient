﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

public class RowPlacer : MonoBehaviour {
	public Button sampleButton;
	private RowContentHolder rch;
	private Button[] allButtons,rows,followButtons;
	private List<Button> rowsList,followButtonsList;
	public GameObject panel;
	public Text title;

	private bool rowsFilled; //determines whether rows have received data or not
	// Use this for initialization
	public void PlaceRows () {
		rowsList = new List<Button> ();
		followButtonsList = new List<Button> ();
		rowsFilled = false;
		Debug.Log ("RowPlacer starting");
		rch = GameObject.FindObjectOfType<RowContentHolder>();
		allButtons = GetComponentsInChildren<Button> ();
		foreach (Button b in allButtons) {
			if (b.name.ToLower ().Contains ("button")) {
				rowsList.Add (b);
			} else if (b.name.ToLower ().Equals ("follow")) {
				followButtonsList.Add (b);
			} else {
				Debug.Log ("wtf!!!");
			}
		}
		rows = rowsList.ToArray ();
		followButtons = followButtonsList.ToArray ();
		float buttonHeight = sampleButton.GetComponent<RectTransform>().rect.height;
		float panelWidth = panel.GetComponent<RectTransform>().rect.width;	
		if (rch.hasSucceeded() && !rowsFilled) {
			int i = 0;
			for (i=0; i<rch.rows.Length;i++){				
				rows [i].GetComponentsInChildren<Text>() [0].text = rch.rows [i].rank.ToString();
 				rows [i].GetComponentsInChildren<Text>() [1].text = rch.rows [i].name;
				rows [i].GetComponentsInChildren<Text>() [2].text = rch.rows [i].score.ToString();
				rows [i].GetComponentsInChildren<Text>() [3].text = rch.rows [i].level.ToString();
				rows [i].GetComponentsInChildren<Text>() [4].text = rch.rows [i].id.ToString();
				if (rch.rows[i].id == Convert.ToInt32(codeMother.cmInstance.myID)) {
					followButtons[i].GetComponentInChildren<Button>().GetComponent<Image>().color = Color.gray;
					followButtons[i].GetComponentInChildren<Button> ().interactable = false;
				} else if(rch.rows[i].iFollowHim==1){					
					followButtons[i].GetComponentInChildren<Button>().GetComponent<Image>().color = Color.green;
				}else if(rch.rows[i].iFollowHim==0){					
					followButtons[i].GetComponentInChildren<Button>().GetComponent<Image>().color = Color.white;
				}
				if (rch.rows [i].me) {
					for (int j = 0; j < 4; j++) {
						rows [i].GetComponentsInChildren<Text> () [j].color = Color.white;
					}
				}				
			}
			//disabling the rest of buttons if less than 20 users are in the lb:
			for(;i<20;i++){
				for (int j = 0; j < 4; j++) {
					rows [i].GetComponentsInChildren<Text> () [j].text = "";
				}
			}
			panel.GetComponent<RectTransform> ().sizeDelta.Set(0, rch.rows.Length * buttonHeight);
			Debug.Log ("rows filled.");
			title.text = rch.lbTitle;
			rowsFilled = true;
			rch.loadingPanel.SetActive(false);	
			rch.lbPanel.SetActive (true);
		}
	}
}